
import org.jboss.netty.channel.*;

public class SettingsServerHandler extends SimpleChannelHandler{
	@Override
	public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) {
		Channel channel = ctx.getChannel();
		SettingsStorage storage = SettingsStorage.instance();
		
		SettingsPacket packet = (SettingsPacket)e.getMessage();
		if (packet == null) return;

		SettingsPacket.Type type = packet.getType();
		String key = packet.getKey();
		String value = packet.getValue();
		
		packet.setType(SettingsPacket.FAIL);
		
		switch (type) {
		case GET:
			value = storage.get(key);
			if (value != null) {
				packet.setType(SettingsPacket.SUCCESS);
				packet.setValue(value);
			}		
			break;
		case SET:
			if (value != null) {
				storage.set(key, value);
				packet.setType(SettingsPacket.SUCCESS);
			}
			break;
		case WATCH:
			if (storage.watch(key, channel)) {
				packet.setType(SettingsPacket.SUCCESS);
			}
			break;
		case UNWATCH:
			if (storage.watch(key, channel)) {
				packet.setType(SettingsPacket.SUCCESS);
			}
			break;
		default:			
		}
		ctx.getChannel().write(packet);
	}
	
	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e) {
		System.out.println("exceptionCaught : " + e.toString());
		e.getCause().printStackTrace();
	}
	
	@Override
	public void channelConnected(ChannelHandlerContext ctx, ChannelStateEvent e) {
		System.out.println("channelConnected : " + ctx.getChannel().getRemoteAddress());
	}
	
	@Override
	public void channelDisconnected(ChannelHandlerContext ctx, ChannelStateEvent e) {
		System.out.println("channelDisconnected : " + ctx.getChannel().getRemoteAddress());
	}
}
