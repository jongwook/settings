
import java.net.InetSocketAddress;
import java.util.Scanner;
import java.util.concurrent.Executors;

import org.jboss.netty.bootstrap.ClientBootstrap;
import org.jboss.netty.channel.*;
import org.jboss.netty.channel.socket.nio.NioClientSocketChannelFactory;

public class SettingsClient {
	public static final int port = 8192;
	
	public static void main(String ... args) {
		// configure the client
		ClientBootstrap bootstrap = new ClientBootstrap(
					new NioClientSocketChannelFactory(
						Executors.newCachedThreadPool(),
						Executors.newCachedThreadPool()
					)
				);
		
		// set up the pipeline
		bootstrap.setPipelineFactory(new ChannelPipelineFactory() {
			public ChannelPipeline getPipeline() throws Exception {
				return Channels.pipeline(new SettingsPacketEncoder(),
										new SettingsPacketDecoder(),
										new SettingsClientHandler());
			}
		});
		
		// connect to the server
		ChannelFuture future = bootstrap.connect(new InetSocketAddress("localhost", port));
		Channel channel = future.awaitUninterruptibly().getChannel();
		if (!future.isSuccess()) {
			future.getCause().printStackTrace();
			bootstrap.releaseExternalResources();
			return;
		}

		ChannelFuture lastWriteFuture = null;
		
		// get user input and send packets
		try (
			Scanner scanner = new Scanner(System.in);
		) {
			while (true) {
				String line = scanner.nextLine();
				if (line == null) break;
				
				String tokens[] = line.split(" ", 3);
				
				SettingsPacket packet;
				
				if(tokens.length == 0) continue;
				
				if(tokens[0].equals("set")) {
					if(tokens.length < 3) {
						System.err.println("[usage] set key value");
						continue;
					}
					packet = new SettingsPacket(SettingsPacket.SET, tokens[1], tokens[2]);
				} else if (tokens[0].equals("get")) {
					if(tokens.length < 2) {
						System.err.println("[usage] get key");
						continue;
					}
					packet = new SettingsPacket(SettingsPacket.GET, tokens[1]);
				} else if (tokens[0].equals("watch")) {
					if(tokens.length < 2) {
						System.err.println("[usage] watch key");
						continue;
					}
					packet = new SettingsPacket(SettingsPacket.WATCH, tokens[1]);
				} else if (tokens[0].equals("unwatch")) {
					if(tokens.length < 2) {
						System.err.println("[usage] unwatch key");
						continue;
					}
					packet = new SettingsPacket(SettingsPacket.UNWATCH, tokens[1]);
				} else if (tokens[0].equals("exit") || tokens[0].equals("quit")) {
					break;
				} else {
					System.err.println("Unknown Command");	
					continue;
				}
				
				lastWriteFuture = channel.write(packet);
			}
		}
	
		if (lastWriteFuture != null) {
			lastWriteFuture.awaitUninterruptibly();
		}
		
		channel.close();
		bootstrap.releaseExternalResources();
	}
}
