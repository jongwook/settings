
import java.net.InetSocketAddress;
import java.util.concurrent.Executors;

import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.channel.*;
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;

public class SettingsServer {
	static final int port = 8192;
	
	public static void main(String ... args) {
		ServerBootstrap bootstrap = new ServerBootstrap(
										new NioServerSocketChannelFactory(
											Executors.newCachedThreadPool(),
											Executors.newCachedThreadPool()));
		
		bootstrap.setPipelineFactory(new ChannelPipelineFactory() {
							public ChannelPipeline getPipeline() throws Exception {
								return Channels.pipeline(new SettingsPacketEncoder(),
														new SettingsPacketDecoder(),
														new SettingsServerHandler());
							}
						});
		
		bootstrap.bind(new InetSocketAddress(port));
	}
}
