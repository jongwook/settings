
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.jboss.netty.channel.Channel;

public class SettingsStorage {
	
	private static SettingsStorage instance = null;
	
	/**
	 * @return the singleton instance of SettingsStorage
	 */
	public static SettingsStorage instance() {
		if (instance == null) {
			instance = new SettingsStorage();
		}
		return instance;
	}
	
	private SettingsStorage() {
		storage = new ConcurrentHashMap<String, String>();
		watchersMap = new ConcurrentHashMap<String, Set<Channel>>();
	}
	
	protected Map<String, String> storage;
	protected Map<String, Set<Channel>> watchersMap;
	
	/**
	 * @param key
	 * @return the value associated with given key
	 */
	public String get(String key) {
		return storage.get(key);
	}
	
	/**
	 * Set the value associated with given key, and notify the change to watchers 
	 * @param key
	 * @param value
	 */
	public void set(String key, String value) {
		SettingsPacket packet = new SettingsPacket(SettingsPacket.NOTIFICATION, key, value);
		
		for (Channel channel : ensureWatchers(key)) {
			channel.write(packet);
		}
		
		storage.put(key, value);
	}
	
	/**
	 * Add a channel watching changes for given key
	 * @param key
	 * @param channel
	 * @return true if the channel was not already watching the key
	 */
	public boolean watch(String key, Channel channel) {
		return ensureWatchers(key).add(channel);
	}
	
	/**
	 * Remove a channel from watchers for given key
	 * @param key
	 * @param channel
	 * @return true if the channel was indeed watching the key
	 */
	public boolean unwatch(String key, Channel channel) {
		return ensureWatchers(key).remove(channel);
	}
	
	/** 
	 * @param key
	 * @return a set of channels associated with given key, creating as necessary
	 */
	private Set<Channel> ensureWatchers(String key) {
		Set<Channel> watchers = watchersMap.get(key);
		if (watchers == null) {
			watchers = Collections.newSetFromMap(new ConcurrentHashMap<Channel, Boolean>());
			watchersMap.put(key, watchers);
		}
		return watchers;
	}
}
