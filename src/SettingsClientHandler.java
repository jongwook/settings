
import org.jboss.netty.channel.*;

public class SettingsClientHandler extends SimpleChannelHandler {
	@Override
	public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) {
		SettingsPacket packet = (SettingsPacket)e.getMessage();
		System.out.println("messageReceived : " + packet.stringify());
	}
	
	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e) {
		System.out.println("exceptionCaught");
	}
	
	@Override
	public void channelConnected(ChannelHandlerContext ctx, ChannelStateEvent e) {
		System.out.println("channelConnected : " + ctx.getName());
	}
	
	@Override
	public void channelDisconnected(ChannelHandlerContext ctx, ChannelStateEvent e) {
		System.out.println("channelDisconnected : " + ctx.getName());
	}
}
