
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.*;
import org.jboss.netty.handler.codec.oneone.OneToOneEncoder;

public class SettingsPacketEncoder extends OneToOneEncoder {

	@Override
	protected Object encode(ChannelHandlerContext ctx, Channel channel, Object msg) throws Exception {
		// do not handle unknown message
		if(!(msg instanceof SettingsPacket)) {
			return msg;
		}
		
		SettingsPacket packet = (SettingsPacket)msg;
		String json = packet.stringify();
		byte jsonBytes[] = json.getBytes("UTF-8");
		int length = jsonBytes.length;
		int capacity = 4 + length; 
		
		ChannelBuffer buffer = ChannelBuffers.buffer(capacity);
		buffer.writeInt(length);
		buffer.writeBytes(jsonBytes);
		return buffer;
	}
}
