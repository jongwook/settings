import com.fasterxml.jackson.databind.ObjectMapper;


/**
 * Plain-Old Java Object (POJO) for a packet
 *
 */
public class SettingsPacket {
	public enum Type {
		GET, SET, WATCH, UNWATCH,	// Client -> Server
		SUCCESS, FAIL, NOTIFICATION // Server -> Client
	}
	
	public static final Type GET = Type.GET;
	public static final Type SET = Type.SET;
	public static final Type WATCH = Type.WATCH;
	public static final Type UNWATCH = Type.UNWATCH;
	public static final Type SUCCESS = Type.SUCCESS;
	public static final Type FAIL = Type.FAIL;
	public static final Type NOTIFICATION = Type.NOTIFICATION;
	
	private Type _type;
	private String _key, _value;
	
	public SettingsPacket() {}
	
	public SettingsPacket(Type type, String key) {
		_type = type;
		_key = key;
	}
	
	public SettingsPacket(Type type, String key, String value) {
		_type = type;
		_key = key;
		_value = value;
	}
		
	public static SettingsPacket parse(String json) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.readValue(json, SettingsPacket.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public String stringify() {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(this);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public Type getType() { return _type; }
	public void setType(Type type) { _type = type; }
	
	public String getKey() { return _key; }
	public void setKey(String key) { _key = key; }
	
	public String getValue() { return _value; }
	public void setValue(String value) { _value = value; }
	
	/**
	 * A test to make sure this class works with Jackson data binding
	 */
	public static void main(String ... args) {
		String json = "{\"type\":\"SET\",\"key\":\"hello\",\"value\":\"world\"}";
		SettingsPacket packet = SettingsPacket.parse(json);
		
		System.out.println(packet.getType() + " " + packet.getKey() + " " + packet.getValue());
		
		System.out.println(packet.stringify());
	}
}
