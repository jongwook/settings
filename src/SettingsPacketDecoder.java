import org.jboss.netty.handler.codec.replay.*;
import org.jboss.netty.channel.*;
import org.jboss.netty.buffer.ChannelBuffer;

public class SettingsPacketDecoder extends ReplayingDecoder<VoidEnum>{
	@Override
	protected Object decode(
            ChannelHandlerContext ctx, Channel channel,
            ChannelBuffer buffer, VoidEnum state) {
		int length = buffer.readInt();
		byte jsonBytes[] = new byte[length];
		buffer.readBytes(jsonBytes);
		
		try {
			String json = new String(jsonBytes, "UTF-8");
			return SettingsPacket.parse(json);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
}
